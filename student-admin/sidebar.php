<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sidebar</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="home.php"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                    <li><a href="stdviews.php"><i class="glyphicon glyphicon-user"></i> All Students</a></li>
                    <li><a href="index.php"><i class="glyphicon glyphicon-book"></i> New Courses</a></li>
                    <li><a href="views.php"><i class="glyphicon glyphicon-record"></i>All Courses</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-cog"></i> Settings</a></li>
                    
                    <li class="submenu">
                         <a href="home.php">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                         </a>
                         <ul>
                            <li><a href="home.php">Login</a></li>
                            <li><a href="index.php">Add Courses</a></li>
                            <li><a href="views.php">Views Courses</a></li>
                            <li><a href="stdviews.php">views Students</a></li>
                            <li><a href="show.php">Students Profile</a></li>
                           
                        </ul>
                    </li>
                </ul>
             </div>
              <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

             </body>
             </html>